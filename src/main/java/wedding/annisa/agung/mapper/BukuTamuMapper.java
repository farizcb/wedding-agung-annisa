package wedding.annisa.agung.mapper;

import org.springframework.stereotype.Component;
import wedding.annisa.agung.entity.BukuTamu;
import wedding.annisa.agung.model.BukuTamuReq;

/**
 * @author muhamad.fariz
 * email : farizcb@gmail.com
 * on 17/08/2020
 */

@Component
public class BukuTamuMapper {

    public BukuTamu fromReq(BukuTamuReq req) {
        BukuTamu bukuTamu = new BukuTamu();
        bukuTamu.setName(req.getNama());
        bukuTamu.setText(req.getText());
        return bukuTamu;
    }

}
