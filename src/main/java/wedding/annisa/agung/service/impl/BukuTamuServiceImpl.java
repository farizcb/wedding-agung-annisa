package wedding.annisa.agung.service.impl;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import wedding.annisa.agung.entity.BukuTamu;
import wedding.annisa.agung.mapper.BukuTamuMapper;
import wedding.annisa.agung.model.BukuTamuReq;
import wedding.annisa.agung.repository.BukuTamuRepository;
import wedding.annisa.agung.service.BukuTamuService;
import wedding.annisa.agung.util.Checks;

import java.util.Date;
import java.util.List;

/**
 * @author muhamad.fariz
 * email : farizcb@gmail.com
 * on 17/08/2020
 */

@Service
@RequiredArgsConstructor
public class BukuTamuServiceImpl implements BukuTamuService {

    private final BukuTamuRepository bukuTamuRepository;
    private final BukuTamuMapper bukuTamuMapper;

    @Override
    public void save(BukuTamuReq bukuTamuReq) {
        // validate mandatory
        Checks.isTrue(StringUtils.isNotBlank(bukuTamuReq.getNama()), "Nama tidak boleh kosong !");
        Checks.isTrue(StringUtils.isNotBlank(bukuTamuReq.getText()), "Ucapan tidak boleh kosong !");

        // convert to entity
        BukuTamu bukuTamu = bukuTamuMapper.fromReq(bukuTamuReq);
        bukuTamu.setCreatedDate(new Date());

        // insert to db
        bukuTamuRepository.save(bukuTamu);
    }

    @Override
    public List<BukuTamu> findAll() {
        return (List<BukuTamu>) bukuTamuRepository.findAll(Sort.by(Sort.Direction.DESC, "createdDate"));
    }
}
