package wedding.annisa.agung.service;

import wedding.annisa.agung.entity.BukuTamu;
import wedding.annisa.agung.model.BukuTamuReq;

import java.util.List;

/**
 * @author muhamad.fariz
 * email : farizcb@gmail.com
 * on 17/08/2020
 */
public interface BukuTamuService {

    void save(BukuTamuReq bukuTamuReq);

    List<BukuTamu> findAll();

}
