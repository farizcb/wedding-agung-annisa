package wedding.annisa.agung.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import wedding.annisa.agung.model.BukuTamuReq;
import wedding.annisa.agung.service.BukuTamuService;
import wedding.annisa.agung.util.JsonResponse;

/**
 * @author muhamad.fariz
 * email : farizcb@gmail.com
 * on 17/08/2020
 */

@RestController
@RequestMapping(value = "buku-tamu", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "Buku Tamu API", produces = MediaType.APPLICATION_JSON_VALUE, tags = {"Buku Tamu"})
@RequiredArgsConstructor
public class BukuTamuController {

    private final BukuTamuService bukuTamuService;

    @PostMapping("simpan")
    @ApiOperation(value = "Simpan ucapan buku tamu", response = JsonResponse.class)
    public ResponseEntity<?> save(@RequestBody BukuTamuReq bukuTamuReq) {
        bukuTamuService.save(bukuTamuReq);
        return ResponseEntity.ok(JsonResponse.ok(null, "Data berhasil disimpan !"));
    }

    @GetMapping("find-all")
    @ApiOperation(value = "Data ucapan buku tamu", response = JsonResponse.class)
    public ResponseEntity<?> findActive() {
        return ResponseEntity.ok(JsonResponse.ok(bukuTamuService.findAll()));
    }

}
