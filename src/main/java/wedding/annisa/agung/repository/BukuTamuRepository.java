package wedding.annisa.agung.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import wedding.annisa.agung.entity.BukuTamu;

/**
 * @author muhamad.fariz
 * email : farizcb@gmail.com
 * on 17/08/2020
 */
public interface BukuTamuRepository extends PagingAndSortingRepository<BukuTamu, String> {
}
