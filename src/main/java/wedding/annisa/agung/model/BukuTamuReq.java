package wedding.annisa.agung.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author muhamad.fariz
 * email : farizcb@gmail.com
 * on 17/08/2020
 */

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BukuTamuReq {

    private String nama;
    private String text;

}
